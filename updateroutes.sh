#!/bin/bash
vtysh -c "show ip bgp paths" | awk '{for (i=1; i<=NF-2; i++) $i = $(i+2); NF-=2; print}' | grep -P "[0-9]" | sed 's/^/76167\ /g' | sort | uniq > ./bgp_paths/76167
python bgp2graph.py
