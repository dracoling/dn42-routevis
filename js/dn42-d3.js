function updateNodes(name, value) {
  return function(d) {
    svg.select("#node-" + d[name].id).classed(name, value);
  };
}
function highlightNode(nid) {
  //graphdata.nodes[id=nid]
  var result = $.grep(graphdata.nodes, function(e){ return e.id == nid; });
  if (result.length == 1)  mouseover(result[0]);
  else {
    showInfo({'color': 'none', 'id': ''});
    svg.selectAll("circle.node")
       .classed("hide", false);
    svg.selectAll("line.link")
       .classed("hide", false);
  }
}
function showInfo(node) {
  var output = '';
  if (node.id != '') {
    output += '<h2 style="color:'+node.color+'">'+node.id+'</h2>\n';
    if (node.neighbors.length < 1) {
      output += '<p>Neighbors: none</p>\n';
    } else {
      var nout = [];
      node.neighbors.forEach(function(e,i,a) { nout.push(graphdata.nodes[e].id); });
      output += '<p>Neighbors:' + nout.join(', ') + '</p>\n';
    }
    output += "<p>Data Source:"+node.datasource.join(', ')+"</p>\n";
  }
  
  document.getElementById('infobox').innerHTML = output
  console.log('node',node);
}
function mouseover(d) {
  showInfo(d);
  svg.selectAll("circle.node")
     .data(graphdata.nodes)
     .classed("hide",function (n,i) { 
       if (n.id == d.id) return false;
       if (d.neighbors.indexOf(i) != -1) return false;
       return true;
       });
  svg.selectAll("line.link")
    .data(graphdata.links)
    .classed("hide",function (n) { 
      if (n.source.id == d.id) return false;
      if (n.target.id == d.id) return false;
      return true;
      });
     
  svg.selectAll("line.link.target-" + d.id)
      .classed("target", true)
      .each(updateNodes("source", true));

  svg.selectAll("line.link.source-" + d.id)
      .classed("source", true)
      .each(updateNodes("target", true));
}
function mouseout(d) {
  showInfo({'color': 'none', 'id': ''});
  svg.selectAll("line.link.source-" + d.id)
      .classed("source", false)
      .each(updateNodes("target", false));

  svg.selectAll("line.link.target-" + d.id)
      .classed("target", false)
      .each(updateNodes("source", false));

  svg.selectAll("circle.node")
     .classed("hide", false);
  svg.selectAll("line.link")
     .classed("hide", false);
}
var collectNeighbors = function (graph) {
  var node = graph.nodes;
  var edge = graph.links;
  edge.forEach(function(e,i,a) {
    var source = node[e.source]['neighbors'] || [];
    var target = node[e.target]['neighbors'] || [];
    source.push(e.target);
    target.push(e.source);
    node[e.source]['neighbors'] = source;
    node[e.target]['neighbors'] = target;
  });
  node.forEach(function(e,i,a) {
    var n = a[i]['neighbors'] || [];
    a[i]['neighbors'] = n;
  });
  return graph;
}

var doColorConnectivity = function () {
  graphdata = getConnectivity(graphdata);
  svg.selectAll("circle.node")
     .data(graphdata.nodes)
     .style("fill", function (d) { 
        if (d.neighbors.length < 1) return 'black';
        else if (d.connectivity < 1) return 'red';
        else if (d.connectivity < 2) return 'yellow';
        else return 'lime';
      });
  svg.selectAll("line.link")
     .data(graphdata.links)
     .style("stroke", function(d) {
       c = Math.min(d.source.connectivity,d.target.connectivity);
       if (c < 1) return 'red';
       else if (c < 2) return 'yellow';
       else return 'lime';
     });
}

var getConnectivity = function (graph) {
  var node = graph.nodes;
  // Poorly connected nodes get an initial connectivity score of 0
  // Recalculate connectivity until stable
  var changed = true;
  var loopmax = 500;
  while (changed) {
    changed = false;
    node.forEach(function(e,i,a) {
      if ((e.connectivity === 0) || (e.connectivity === 1)) { /* next! */ }
      else if (e.neighbors.length == 0 ) {
        node[i]['connectivity'] = 0;
        // this won't change anyone else's connectivity, we have no neighbors.
      }
      else if (e.neighbors.length == 1) {
        node[i]['connectivity'] = 0;
        changed = true;
      } 
      else {
        var goodNodes = 0;
        e.neighbors.forEach(function (n, index, array) { 
          var c = node[n].connectivity || (node[n].neighbors.length);
          if (c >= 2) goodNodes += 1;
        });
        if (goodNodes < 2) {
          node[i]['connectivity'] = 1;
          changed = true;
        } 
        else {
          node[i]['connectivity'] = goodNodes;
        }
      }
    });
    
    if (--loopmax == 0) {
      console.log("Breaking loop!");
      console.log(graph)
      break; // something went wrong if we got here
    }
  }
  return graph;
}