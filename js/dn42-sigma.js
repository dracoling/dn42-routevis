function makeNode(element, index, array) {
  var data = element;
  var node = {'label': data['name'], 'size': 10, 'x':Math.random(), 'y':Math.random(), 'color': 'lime'};
  var sourcenodes = ['76167', '76198'];
  if ((sourcenodes.indexOf(data['name']) != -1)) { 
    node['note'] = "Route graph information source";
  }
  sigInst.addNode(data['name'], node); 
}
function makeEdge(element, index, array) {
  var data = element;
  var nodeExists;
  try { nodeExists = sigInst.getEdges('AS'+data['source'] + 'AS'+data['target']) } catch (e) { nodeExists = false; };
  if (!nodeExists) {
    sigInst.addEdge('AS'+data['source'] + 'AS'+data['target'],data['source'],data['target']);
  }
}
function fillGraph(data) {
  data['nodes'].forEach(makeNode);
  data['links'].forEach(makeEdge);
}
function colorGraph() {
  // This section is for red-coloring badly connected nodes (only 1 peer)
  var redNodes = [];
  sigInst.iterNodes(function(node) { 
    // This breaks the attributes into things
    node.attr['datasource'] = node.attr['attributes'][1]['val'];
    node.attr['betweenness'] = node.attr['attributes'][0]['val'] || 0;
    node.color = 'lime';
    node.size = 5 + (10 * node.attr['attributes'][0]['val']);

    var edges = [];
    sigInst.iterEdges(function (e) { 
      if ((e.source == node.id) || (e.target == node.id)) { 
        edges.push(e.id);
      } 
    });
    if (edges.length < 2) { 
      redNodes.push(node.id);
      if (edges.length < 1 ) {
        node.color = 'black';
      } else {
        node.color = 'red';
      }
    }
  });
  // end of reddening
  // This section is for yellow-coloring poorly connected nodes (only 1 well-connected peer, all other peers singly-connected)
  var changed = true;
  while (changed) {
    changed = false;
    sigInst.iterNodes(function(node) {
      if ((node.color == 'red') || (node.color == 'yellow') || (node.color == 'black')) {
        // do nothing
      } 
      else {
        var neighbors = [];
        sigInst.iterEdges(function (e) { 
          if ((e.source == node.id)) {
            neighbors.push(sigInst.getNodes(e.target)); 
          }
          if (e.target == node.id) { 
            neighbors.push(sigInst.getNodes(e.source)); 
          }
        });
        var goodNodes = 0;
        var badNodes = 0;
        neighbors.forEach(function (element, index, array) { 
          if (element.color == 'red') badNodes += 1;
          else if (element.color == 'yellow') badNodes += 1;
          else goodNodes += 1;
        });
        if (goodNodes < 2) {
          node.color = 'yellow'
          changed = true;
        }
      }
    });
  }
  // end of yellowing
  // Update edge coloring
  sigInst.iterEdges(function(e) {
    var source = sigInst.getNodes(e.source);
    var target = sigInst.getNodes(e.target);
    if ((source.color == 'red') || (target.color == 'red')) {
      e.color = 'red';
    } else if ((source.color == 'yellow') || (target.color == 'yellow')) {
      e.color = 'yellow';      
    } else {
      e.color = 'green';
    }
  });
  // end edge update
}

// nodes is an array of node IDs
function doHilight(nodes) {
  var cur = sigInst.getNodes(nodes[0]);
  var output = box_header(cur.label,cur.color);
  output += '<p>Peers:';  
  
  var greyColor = '#666';
  var neighbors = {};
  sigInst.iterEdges(function(e) {
    if (nodes.indexOf(e.source) < 0 && nodes.indexOf(e.target) < 0) {
      if (!e.attr['grey']) {
        e.attr['true_color'] = e.color;
        e.color = greyColor;
        e.attr['grey'] = 1;
      }
    } else {
      e.color = e.attr['grey'] ? e.attr['true_color'] : e.color;
      e.attr['grey'] = 0;

      neighbors[e.source] = 1;
      neighbors[e.target] = 1;
    }
  }).iterNodes(function(n) {
    if (!neighbors[n.id]) {
      if (!n.attr['grey']) {
        n.attr['true_color'] = n.color;
        n.color = greyColor;
        n.attr['grey'] = 1;
      }
    } else {
      n.color = n.attr['grey'] ? n.attr['true_color'] : n.color;
      n.attr['grey'] = 0;
      if (!(n.id == nodes[0])) {
        output += '<span class="peer-'+n.color+'">'+n.label+'</span> ';
      }
    }
  }).draw(2, 2, 2);
  output += "</p>";
  output += "<p>Sources:" + cur.attr['datasource'] + "</p>";
  if (cur.attr['note']) {
    output += "</p><p>" + cur.attr['note'];
  }
  updateInfoBox(output + '</p>');  
  
}
function noHilight() {
  updateInfoBox('');
  sigInst.iterEdges(function(e) {
    e.color = e.attr['grey'] ? e.attr['true_color'] : e.color;
    e.attr['grey'] = 0;
  }).iterNodes(function(n) {
    n.color = n.attr['grey'] ? n.attr['true_color'] : n.color;
    n.attr['grey'] = 0;
  }).draw(2, 2, 2);
}

var box_header = function (text,color) { 
color = color || 'orange';
return '<h2 style="color: '+color+';">' + text +'</h2>';
}
function updateInfoBox(stuff) {
  var infobox = document.getElementById('infobox');
  infobox.innerHTML = stuff;
}

function init() {
//  fillGraph(data);
  colorGraph();
  sigInst.startForceAtlas2();
  var isRunning = true;
  document.getElementById('stop-layout').addEventListener('click',function(){
    if(isRunning){
      isRunning = false;
      sigInst.stopForceAtlas2();
      document.getElementById('stop-layout').childNodes[0].nodeValue = 'Start Layout';
      // hover hilight
      sigInst.bind('overnodes', function(event) {
        var nodes = event.content;
        doHilight(event.content);
      }).bind('outnodes', function() {
        noHilight();
      });
      // end hover hilight
    }else{
      isRunning = true;
      // disable hover hilight
      sigInst.unbind('overnodes').unbind('outnodes');
      // end disable
      sigInst.startForceAtlas2();
      document.getElementById('stop-layout').childNodes[0].nodeValue = 'Stop Layout';
    }
  },true);
  document.getElementById('rescale-graph').addEventListener('click',function(){
    sigInst.position(0,0,1).draw();
  },true);
  
}
