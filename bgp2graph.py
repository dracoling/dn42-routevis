import os
import string
import sys
sys.path.append("networkx-1.7-py2.6.egg")
import networkx as netx

graph = netx.Graph()

def addNode(num,source):
  global graph
  if num not in graph.node:
    graph.add_node(num)
    graph.node[num].setdefault('datasource', [source])
  else:
    if source not in graph.node[num].setdefault('datasource', []):
      graph.node[num]['datasource'].append(source)

def addroute(line,source = 'self'):
  global graph
  route = string.split(line)
  old = ""
  for num in route:
    if (old == ""):
      #first route!
      old = num
      addNode(num,source)
      next
    if (num == old):
      #print 'ignoring ' + str(num)
      next
    else:
      addNode(num,source)
      graph.add_edge(old,num)
      
      if source not in graph[old][num].setdefault('datasource', []):
        graph[old][num]['datasource'].append(source)
      old = num


for filename in os.listdir('./bgp_paths'):
#  print "getting lines for " + filename
  lines = [line.strip() for line in open('./bgp_paths/'+filename)]  
  for line in lines:
    addroute(line,filename)

# Calculate all of the betweenness measures
betweenness = netx.algorithms.centrality.betweenness_centrality(graph)
for num in graph.nodes():
  graph.node[num]['betweenness'] = betweenness[num]

# can export json before cleanup
import json
from networkx.readwrite import json_graph
json.dump(json_graph.node_link_data(graph), open('dn42.json','w'))

# Need to clean up sources before GEXF export
for num in graph.nodes():
  graph.node[num]['datasource'] = string.join(graph.node[num]['datasource'],', ')
for a,b in graph.edges():
  graph[a][b]['datasource'] = string.join(graph[a][b]['datasource'],', ')

# Perform GEXF export
netx.write_gexf(graph,"./dn42.gexf")
